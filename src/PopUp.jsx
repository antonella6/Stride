import React from 'react';
import { useState } from 'react';
import './PopUp.css';

/**
 * Functional component that shows a pop up to create a new task in the main component.
 * 
 * @param {reactHookObjectSetter} setTasks- A hook setter to change the tasks list. Recieves a object.
 * @callback handleClose - A method that closes the popup. Doesen't recieve anything.
 *  
 * @return a functional component that shows a popup window.
 */
const PopUp = ({ handleClose, setTasks }) => {
  
  const [inputValue, setInputValue] = useState({});

  /**
   * A function the adds the new task writed in the input box  and close the pop up window.
   */
  const saveValueAndClosePopUp = () => {
    setTasks((tasksArray) => [...tasksArray, inputValue]);
    handleClose();
  };

  /**
 * A function that changes the value of the input value hook.
 * 
* @param {object} event - The captured event from the input box.
 */
  const inputValueChanged = (event) => {
    setInputValue({ value: event.target.value, checked: false });
  };

  /**
   * Function that closes the popup window if enter button is pressed.
   * 
   * @param {object} event - The captured event from the input box after pressing a key of the  window.
   */
  const enterButtonPressed = (event) => {
    event.key === 'Enter' && saveValueAndClosePopUp();
  };

    /**
   * A function that closes the popup window if clicked outside of the popup box.
   * The window object represents an open window in a browser.
   * 
   * @param {object} event - The captured event from the mouse click.
   */
  window.onclick = (event) => {
    if (event.target.className === 'popup-box') {
      handleClose();
    }
  };

  return (
    <div className="popup-box">
      <div className="box">
        <input
          className="input"
          type="text"
          autoFocus
          onChange={inputValueChanged}
          placeholder="Inserisci voce"
          onKeyUp={enterButtonPressed}
        ></input>
        <div>
          <button className="button-salva" onClick={saveValueAndClosePopUp}>
            Salva
          </button>
        </div>
      </div>
    </div>
  );
};

export { PopUp };
