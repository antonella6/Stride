import React from 'react';
import './PopUpTitle.css';

/**
 * Functional component that shows a pop up to change the title in the main component.
 * 
 * @param {reactHookStringSetter} setTitle - A hook setter to change the main title. Recieves a string.
 * @callback handleClose - A method that closes the title popup. Doesen't recieve anything.
 *  
 * @return a functional component that shows a popup window.
 */
const PopUpTitle = ({ setTitle, handleClose }) => {
  
  const changeTitleAndClosePopUp = (event) => {
    event.preventDefault();
    if (event.target['titleInput'].value.length !== 0) {
      setTitle(event.target['titleInput'].value);
    } else {
      setTitle('Titolo');
    }

    handleClose();
  };

  /**
   * Function that closes the popup title window if enter button is pressed.
   * 
   * @param {object} event - The captured event from the input box after pressing a key of the change title window.
   */
  const enterButtonPressed = (event) => {
    event.key === 'Enter' && changeTitleAndClosePopUp();
  };

  /**
   * A function that closes the popup title window if clicked outside of the popup box
   * The window object represents an open window in a browser.
   * 
   * @param {object} event - The captured event from the mouse click.
   */
  window.onclick = (event) => {
    if (event.target.className === 'title-popup-box') {
      handleClose();
    }
  };

  return (
    <div className="title-popup-box">
      <div className="title-box">
        <form onSubmit={changeTitleAndClosePopUp}>
          <input
            className="title-input"
            type="text"
            autoFocus
            name="titleInput"
            placeholder="Inserisci titolo"
            onKeyUp={enterButtonPressed}
          ></input>
          <div>
            <button className="title-button-salva">Salva</button>
          </div>
        </form>
      </div>
    </div>
  );
};

export { PopUpTitle };
