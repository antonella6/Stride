import './App.css';
import React, { useState, useEffect } from 'react';
import { PopUp } from './PopUp';
import { PopUpTitle } from './PopUpTitle';
import Off from './icons/Off.svg';
import On from './icons/On.svg';
import Bin from './icons/Bin.svg';
import Adds from './icons/Adds.svg';


/**
 * Main functional component. Produces a todo-list.
 * 
 * @returns the main functional component
 * 
 */
const App = () => {
  
  const [isOpen, setIsOpen] = useState({ newTask: false, title: false });

  const [title, setTitle] = useState('Titolo');
  
  const [tasks, setTasks] = useState([
    {
      value:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Enim non risus quis mauris aliquam quis',
      checked: false,
    },
    {
      value:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Enim non risus quis mauris aliquam quis',
      checked: false,
    },
  ]);

  const [tempTasks, setTempTasks] = useState([
    {
      value:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Enim non risus quis mauris aliquam quis',
      checked: false,
    },
    {
      value:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Enim non risus quis mauris aliquam quis',
      checked: false,
    },
  ]);

  const [isSearch, setIsSearch] = useState(false);

  useEffect(() => {
    if (!isSearch) {
      setTempTasks(tasks);
    }
  }, [tasks, isSearch]);

  /**
   * A function that shows or hides the pop up new task window.
   */
  const changePopUpNewTaskStatus = () => {
    setIsOpen((openStatus) => ({
      ...openStatus,
      newTask: !openStatus.newTask,
    }));
  };
  
/**
 * A function that shows or hides the pop up title window.
 */
  const changePopUpTitleStatus = () => {
    setIsOpen((openStatus) => ({
      ...openStatus,
      title: !openStatus.title,
    }));
  };

/**
 * A function that deletes the selected task from the task list.
 * 
 * @param {object} event - The captured event from the pressed bin-button.
 */
  const deleteItem = (event) => {
    const idToDelete = parseInt(event.target.id);
    const tasksWithoutitem = tasks.filter(
      (element, index) => index !== idToDelete
    );
    setTasks(tasksWithoutitem);
  };

/**
 * A function that changes the check-box status and set the task into strikethrough text if checked.
 * 
 * @param {object} event - The captured event from the pressed check-box button.
 */
  const checkItem = (event) => {
    const idToCheck = parseInt(event.target.id);
    let tempArray = [...tasks];
    tempArray[idToCheck].checked = !tasks[idToCheck].checked;

    setTasks(tempArray);
  };

  /**
   * A function that shows the tasks filtered from the input.
   * 
   * @param {object} event - The captured event from the input box.
   */
  const searchTask = (event) => {
    if (event.target.value.length !== 0) {
      setIsSearch(true);
      const filteredTasks = tempTasks.filter((task) =>
        task.value.includes(event.target.value)
      );
      setTasks(filteredTasks);
    } else {
      setTasks(tempTasks);
      setIsSearch(false);
    }
  };

  return (
    <>
      <div className="main">
        <h1 className="title tooltip" onClick={changePopUpTitleStatus}>
          {title}
          <span class="tooltiptext">Cambia titolo</span>
        </h1>

        {tasks
          .sort((a, b) => Number(a.checked) - Number(b.checked))
          .map((item, index) => {
            return (
              <div className="tasks" key={index}>
                <button className="task-button" onClick={deleteItem}>
                  <img src={Bin} alt="Logo-Bin" id={index}></img>
                </button>
                <button className="task-button" onClick={checkItem} id={index}>
                  <img
                    src={item.checked ? On : Off}
                    alt="Logos"
                    id={index}
                  ></img>
                </button>
                {
                  <p className={item.checked ? 'label-barrato' : 'label'}>
                    {item.value}
                  </p>
                }
              </div>
            );
          })}
      </div>

      <div>
        <button className="add-button" onClick={changePopUpNewTaskStatus}>
          <img src={Adds} alt="LogoAdd" className="add-logo"></img>
          Nuova voce
        </button>
        {isOpen.newTask && (
          <PopUp setTasks={setTasks} handleClose={changePopUpNewTaskStatus} />
        )}
        {isOpen.title && (
          <PopUpTitle setTitle={setTitle} handleClose={changePopUpTitleStatus} />
        )}
      </div>

      <div className="search-box">
        <input
          onChange={searchTask}
          placeholder="Cerca voce"
          className="label search-input"
        ></input>
      </div>
    </>
  );
};

export { App };
